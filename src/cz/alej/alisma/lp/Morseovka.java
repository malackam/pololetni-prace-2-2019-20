package cz.alej.alisma.lp;

/*
 * Tento program ma prepisovat text do Morseovy abecedy.
 *
 * Zatim ale umi jen nacitat jednotlive znaky na vstupu a
 * preskakovat tzv. bile znaky (mezery, znaky noveho radku
 * apod.).
 *
 * Doplnte implementaci metody prevedZnak, aby umela prevest
 * alespon vsechna pismena. Vyuzijte vhodnou tridu z Java
 * Collections pro ulozeni vlastniho mapovani mezi znakem
 * a jeho morseovou reprezentaci.
 *
 * Pri vstupu "Ahoj svete" ocekavame vystup
 *
 * .-/..../---/.---/.../...-/./-/./
 */

import java.util.Scanner;
import java.util.HashMap;

public class Morseovka {
    private static final String ODDELOVAC = ".|\\n";
    private static final String NEZNAME = "??";
    private static final HashMap<Character, String> MORSEOVKA = new HashMap<Character, String>();

    private static String prevedZnak(Character znak) {
    	znak = Character.toLowerCase(znak);
    	znak = odstranDiakritiku(znak);
    	if (Character.isWhitespace(znak)) {
            return "";
        }else if (MORSEOVKA.containsKey(znak)) {        	
        	return MORSEOVKA.get(znak);
        }
        return NEZNAME;
    }
    
    private static char odstranDiakritiku(char znak) {
    	switch (znak) {
	    	case '�':
	    		return 'a';
	    	case '�':
	    		return 'e';
	    	case '�':
	    		return 'i';
	    	case '�':
	    		return 'o';
	    	case '�':
	    		return 'u';
	    	case '�':
	    		return 'y';
	    	case '�':
	    		return 'c';
	    	case '�':
	    		return 'd';
	    	case '�':
	    		return 'e';
	    	case '�':
	    		return 'n';
	    	case '�':
	    		return 'r';
	    	case '�':
	    		return 's';
	    	case '�':
	    		return 't';
	    	case '�':
	    		return 'z';
	    	case '�':
	    		return 'u';
    	}
    	return znak;
    }
    
    private static void vytvorMorseovku() {
    	MORSEOVKA.put('a', ".-");
    	MORSEOVKA.put('b', "-...");
    	MORSEOVKA.put('c', "-.-.");
    	MORSEOVKA.put('d', "-..");
    	MORSEOVKA.put('e', ".");
    	MORSEOVKA.put('f', "..-.");
    	MORSEOVKA.put('g', "--.");
    	MORSEOVKA.put('h', "....");
    	MORSEOVKA.put('i', "..");
    	MORSEOVKA.put('j', ".---");
    	MORSEOVKA.put('k', "-.-");
    	MORSEOVKA.put('l', ".-..");
    	MORSEOVKA.put('m', "--");
    	MORSEOVKA.put('n', "-.");
    	MORSEOVKA.put('o', "---");
    	MORSEOVKA.put('p', ".--.");
    	MORSEOVKA.put('q', "");
    	MORSEOVKA.put('r', ".-.");
    	MORSEOVKA.put('s', "...");
    	MORSEOVKA.put('t', "-");
    	MORSEOVKA.put('u', "..-");
    	MORSEOVKA.put('v', "...-");
    	MORSEOVKA.put('w', ".--");
    	MORSEOVKA.put('x', "-..-");
    	MORSEOVKA.put('y', "-.--");
    	MORSEOVKA.put('z', "--..");
    	MORSEOVKA.put('0', "-----");
    	MORSEOVKA.put('1', ".----");
    	MORSEOVKA.put('2', "..---");
    	MORSEOVKA.put('3', "...--");
    	MORSEOVKA.put('4', "....-");
    	MORSEOVKA.put('5', ".....");
    	MORSEOVKA.put('6', "-....");
    	MORSEOVKA.put('7', "--...");
    	MORSEOVKA.put('8', "---..");
    	MORSEOVKA.put('9', "----.");
    	MORSEOVKA.put('.', "");
    }

    public static void main(String[] args) {
    	vytvorMorseovku();
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("");
        boolean pismenoCH = false;
        while (sc.hasNext(ODDELOVAC)) {
            char znak = sc.next(ODDELOVAC).charAt(0);
            
            //Vytvoreni pismena ch
            if (pismenoCH) {
            	pismenoCH = false;
            	if (znak == 'H' || znak == 'h') {
            		System.out.print("----/");
            		continue;
            	}else {
            		System.out.printf("%s/", prevedZnak('c'));
            	}
            }
            if (znak == 'C' || znak == 'c') {
            	pismenoCH = true;
            	continue;
            }            
            
            String morse = prevedZnak(znak);
            
            System.out.printf("%s/", morse);
        }
        sc.close();
        
        //Bylo-li posledni 'C', nevypsalo se (program cekal, zda se z nej nestane ch). Je tedy nutno ho vypsat ted.
        if (pismenoCH) {
        	System.out.printf("%s/", prevedZnak('c'));
        }
        
        System.out.println();
    }

}
